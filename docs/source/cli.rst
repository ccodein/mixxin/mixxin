CLI
===

When installing the MXXN framework, a *CLI* programm is registered as
console scripts under the name *mxxr*. This program simplifies the 
development process and provides functions to manage the installation.

.. note::

    To avoid errors, only a few command line options are available in
    productive use. Only if the extra_require option *develop* is installed,
    the features for the application development are available.

run
---
The run command can be used to start the application with the Uvicorn
application server.

.. note::
    Launching the application via mxxr is only recommended for development
    purposes or for simple applications with few requests and users. 
    The regular application in productive use should be started via 
    the Gunicorn application server.

**Available:** always

.. code-block::

    $ mxxr run --help
    usage: mxxr run [--host HOST] [--port PORT] [--workers WORKERS] [--log-level {critical,error,warning,info,debug,trace}]

    options:
        --help                Show this help message and exit.
        --host HOST           Bind the socket to this host. (default: 127.0.0.1)
        --port PORT           Bind the socket to this port. (default: 8000)
        --workers WORKERS     Number of worker processes. (default: 1)
        --log-level {critical,error,warning,info,debug,trace}
                    The log level. (default: warning)

dev
---
The dev section contains commands to simplify the development process.

watch
+++++
The watch command monitors the files in the given environment package and
restarts the server in case of changes to files.

.. code:: 

    $ mxxr dev watch --help
    usage: mxxr dev watch [--help] [--host HOST] [--port PORT] [--log_level {critical,error,warning,info,debug,trace}] env_pkg

    positional arguments:
        env_pkg               The name of the "mxxn" environment package to be watched(e.g. mxxn, mxnapp).

    options:
        --help                Show this help message and exit.
        --host HOST           Bind the socket to this host. (default: 127.0.0.1)
        --port PORT           Bind the socket to this port. (default: 8000)
        --log_level {critical,error,warning,info,debug,trace}
                            The log level. (default: warning)

db
--
This argument can be used to manage the database schema of the Mxxn,
Mxn or MxnApp packages. For this purpose, a reduced selection of Alembic command
line functions has been added as CLI argument 'mxxr db'.

**Available:** development mode

For more information use the following command:

.. code-block::

   $ mxxr db --help
   usage: mxxr db [--help]
   {init,upgrade,downgrade,branches,current,heads,history,merge,show,revision}

    positional arguments:
      {init,upgrade,downgrade,branches,current,heads,history,merge,show,revision}

        init        Initialize the mxn or mxnapp branch.
        upgrade     Upgrade to a later version.
        downgrade   Revert to a previous version.
        branches    Show current branch points.
        current     Display the current revision for a database.
        heads       Show current available heads in the script directory.
        history     List changeset scripts in chronological order.
        merge       Merge two revisions together. Creates a new migration file.
        show        Show the revision(s) denoted by the given symbol.
        revision    Create a new revision file.

    options:
      --help        Show this help message and exit.

