"""CLI application for the MXXN framework."""
from argparse import ArgumentParser
from mxxn.cli import dev, run
import sys


parser = ArgumentParser(
    add_help=False, description='The cli for MXXN management.')
parser.add_argument(
    '--help', action='help', help='Show this help message and exit.')
subparsers = parser.add_subparsers()
run.add_parser(subparsers)
dev.add_parser(subparsers)


def main() -> None:
    """
    CLI script entry point.

    Raises:
        SystemExit: The program exit exception.
    """
    try:
        args = parser.parse_args()
        args.func(args)

    except Exception as e:
        print('ERROR: ' + str(e))

        sys.exit(1)
