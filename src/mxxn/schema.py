"""The schema module."""
from pydantic import BaseModel


class Schema(BaseModel):
    """The base class for all schemas."""
