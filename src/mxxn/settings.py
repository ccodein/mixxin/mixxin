"""
The Settings module is used to access the application settings.

The settings can be passed via an TOML file or by environment variables.
The following sequence applies: environment variables overwrite the
default values and variables from the TOML file overwrite the data of
the environment variables. The names of the environment variables
correspond to those of the Settings fields, but they have the prefix
*MXXN* and are capitalized (e.g. *MXXN_DATA_PATH*)
"""
from pathlib import Path
from os import environ
from typing import List, Dict, Any
from falcon.asgi import Request, Response
from mxxn.exceptions import filesys as filesys_ex
from mxxn.exceptions import settings as settings_ex
from pydantic import (
        BaseSettings, validator, ValidationError, Extra, DirectoryPath)
import tomli


class Settings(BaseSettings):
    """
    The settings class of the framework.

    This class can be used to access the settings in the
    settings file. For application settings that were not
    set in the settings file, a default value is set so that
    the application can also be started without a settings file.
    """

    app_path: DirectoryPath | None
    """
    The the application path.

    The application path is the location where the runtime data of the
    application are stored. This is usually where the settings.ini and
    the data folder are located, which contains, for example, the SQLite
    database, if used. If the *app_path* variable of the settings file is
    not set, the current working directory at the time of the application
    start is returned.
    """

    data_path: Path | None
    """
    Get the data path.

    The Data folder is normally located in the application path and
    contains, for example, the SQLite database, if one is used. The files
    folder, in which the uploaded files are stored, is also located there.
    If the data_path variable of the settings file is not set, the
    *app_path/data* is returned.
    """

    enabled_mxns: List[str] | None
    """
    The list of enabled mxns.

    It is possible to activate only specific Mxns in the
    settings file. If the *enabled_mxns* variable of the settings
    file is not set, None is returned. To deactivate all installed mxns,
    an empty list can be set in the settings file.
    """

    database_url: str | None
    """
    The the SQLAlchemy database URL.

    If this was not set, the default URL
    *sqlite+aiosqlite://<data_path>/mxxn.db* is returned.

    Returns:
        The default database URL.
    """

    class Config:
        """The config class of the Settings class."""

        extra = Extra.forbid
        env_prefix = 'mxxn_'

    @validator('app_path', pre=True, always=True)
    def app_path_default(
            cls: 'Settings',
            value: DirectoryPath | None) -> DirectoryPath:
        """
        Set the default app_path.

        Args:
            cls: The Settings class.
            value: The value of the app_path.

        Returns:
            The default for the app_path
        """
        if not value:
            return Path.cwd()

        return value

    @validator('data_path', pre=True, always=True)
    def data_path_default(
            cls: 'Settings',
            value: DirectoryPath | None,
            values: Dict[str, Any]) -> Path:
        """
        Set the default data_path.

        Args:
            cls: The Settings class.
            value: The value of the app_path.
            values: The list of set fields.

        Returns:
            The default for the data_path
        """
        if 'app_path' in values and not value:
            return values['app_path']/'data'

        return value

    @validator('database_url', pre=True, always=True)
    def sqlalchemy_url_default(
            cls: 'Settings',
            value: str | None,
            values: Dict[str, Any]) -> str:
        """
        Set the default database_url.

        Args:
            cls: The Settings class.
            value: The value of the database_url.
            values: The list of set fields.

        Returns:
            The default for the database_url
        """
        if 'data_path' in values and not value:
            return f'sqlite+aiosqlite://{values["data_path"]}/mxxn.db'

        return value


def _find_file() -> Path | None:
    """
    Find the settings file.

    The function checks if the environment variable *MXXN_SETTINGS* has
    been set and whether the file exists in the file system. If the
    environment variable has not been set, the current working directory
    is searched for a file with the name *settings.toml*. If this file does
    not exist as well, None is returned.

    Raises:
        mxxn.exceptions.filesys.FileNotExistError: If the file in the
            environment variable MXXN_SETTINGS does not exist.

    Returns:
        The absolute path to the settings file or None if it does
            not exist.
    """
    if 'MXXN_SETTINGS' in environ:
        if Path(environ['MXXN_SETTINGS']).is_file():
            return Path(environ['MXXN_SETTINGS']).resolve()
        else:
            raise filesys_ex.FileNotExistError(
                    'The settings file in the environment variable '
                    'MXXN_SETTINGS does not exist.'
            )
    else:
        if (Path.cwd()/'settings.toml').is_file():
            return Path.cwd()/'settings.toml'

        return None


def load(settings_file: Path | None = None) -> Settings:
    """
    Load the settings from the settings file.

    The function reads the *mxxn* section of the settings file and returns
    a Setting class instance. After reading, the data will be validated.
    If no settings file was passed, the environment variable *MXXN_SETTINGS*
    will be checked. If the environment variable has not been set, the current
    working directory is searched for a file with the name *settings.toml*. If
    this file does not exist as well, a default Settings instance will
    be returned.

    Args:
        settings_file: The settings file.

    Raises:
        mxxn.exceptions.filesys.FileNotExistError: If the passed file
            does not exist.
        mxxn.exceptions.settings.SettingsFormatError: If the passed file
            is not in correct format.
    """
    if not settings_file:
        settings_file = _find_file()

    if settings_file.exists():
        try:
            with open(settings_file, 'rb') as f:
                settings = tomli.load(f)

            if 'mxxn' in settings:
                settings = settings['mxxn']

            return Settings(**settings)

        except tomli.TOMLDecodeError as ex:
            raise settings_ex.SettingsFormatError(
                f'The settings file is not in the required format: {ex}')

        except ValidationError as ex:
            raise settings_ex.SettingsFormatError(ex)

    else:
        raise filesys_ex.FileNotExistError(
                f'The settings file {settings_file} does not exist.')

    return Settings()


class SettingsMiddleware():
    """The middleware for the Settings class."""

    def __init__(self, settings: Settings) -> None:
        """
        Initialize the SettingsMiddleware instance.

        Args:
            settings: The settings of the application.
        """
        self._settings = settings

    async def process_request(self, req: Request, resp: Response) -> None:
        """
        Add the application settings to the request object.

        The settings can be accessed via the context variable
        req.context.settings.

        Args:
            req: The request object.
            resp: The response object.
        """
        req.context.settings = self._settings
