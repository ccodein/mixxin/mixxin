"""Tests for the database module."""
from unittest.mock import patch, PropertyMock
from falcon import runs_sync
from sqlalchemy.ext.asyncio.engine import AsyncEngine
from sqlalchemy.ext.asyncio.scoping import async_scoped_session
import pytest
from mxxn.database import Database
from mxxn.settings import Settings
from mxxn.exceptions import database as database_ex


class TestDatabaseInit():
    """Tests for the __init__ method for the Database class."""

    def test_async_engine(self):
        """Is a async engine."""
        settings = Settings(database_url='sqlite+aiosqlite://')
        db = Database(settings)

        assert isinstance(db.engine, AsyncEngine)

    def test_wrong_dialect(self):
        """Worng dialect in URL."""
        settings = Settings(database_url='xxyyzz://')

        with pytest.raises(database_ex.URLError):
            Database(settings)

    def test_wrong_url_string(self):
        """Worng URL."""
        settings = Settings(
            database_url='sqlite+aiosqlite://%(here)s/database.sqlite')

        with pytest.raises(database_ex.URLError):
            Database(settings)

    def test_is_async_scoped_session(self):
        """Is a async scoped session."""
        settings = Settings(database_url='sqlite+aiosqlite://')
        db = Database(settings)

        assert isinstance(db.session, async_scoped_session)

    @runs_sync
    async def test_engine_bound_to_session(self):
        """Engine was bound to the session."""
        settings = Settings(database_url='sqlite+aiosqlite://')
        db = Database(settings)

        assert db.session.bind == db.engine
